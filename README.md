# Webdav

A deployment of Taiga
https://taiga.io/

## Usage

run `make` for usage

## User Access

run `make proxy` to forward apiserver to port `http://127.0.0.1:8001`

Taiga: http://localhost:8001/api/v1/namespaces/taiga/services/taiga/proxy/
